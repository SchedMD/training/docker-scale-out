export LC_ALL=C.UTF-8
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/
export LD_LIBRARY_PATH=/usr/local/lib64:/usr/local/lib
export PATH=/home/$USER/.local/bin:/home/$USER/bin:/usr/local/bin/:/usr/local/sbin/:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin
